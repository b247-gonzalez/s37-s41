const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name must e provided"]
    },
    lastName : {
        type : String,
        required : [true, "Last name must be provided"]
    },
    email : {
        type : String,
        required : [true, "Email must be provided"]
    },
    password : {
        type : String,
        required : [true, "Password must be provided"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNumber : {
        type : String,
        required : [true, "Mobile number must be provided"]
    },
    enrollments : [
        {
            course_id : {
                type : String,
                required : [true, "Course Id must be provided"]
            },
            enrolledOn : {
                type : Date,
                default : new Date()
            },
            status : {
                type: String,
                default : "Enrolled"
            }
        }
    ]
});

module.exports = mongoose.model("User", userSchema);