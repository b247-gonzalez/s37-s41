const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

/* My code
router.post("/addCourse", auth.verifyAdmin, (req, res) => {
    courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
});
*/

router.post("/addCourse", auth.verify, (req, res) => {
    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});

router.get("/all", (req, res) => {
    courseController.getAllCourse().then(resultFromController => res.send(resultFromController));
});

router.get("/active", (req, res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

router.get("/:courseId/details", (req, res) => {
    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:id/update", auth.verify, (req, res) => {
    courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

router.put("/:id/archive", auth.verify, (req, res) => {
    courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;