const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email : reqBody.email}).then(rslt => {
        if (rslt.length > 0) {
            return true;
        }
        else {
            return false;
        }
    });
};

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNumber : reqBody.mobileNumber,
        password : bcrypt.hashSync(reqBody.password, 10)
    });
    
    return newUser.save().then((user, err) => {
        if (err) {
            return false;
        }
        else {
            return true;
        }
    });
};

module.exports.loginUser = (reqBody) => {
    return User.findOne({email : reqBody.email}).then(rslt => {
        if (rslt == null) {
            return false;
        }
        else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, rslt.password);
            
            if (isPasswordCorrect) {
                return { access : auth.createAccessToken(rslt) };
            }
            else {
                return false;
            }
        }
    });
};

module.exports.getProfile = (data) => {
    return User.findById(data.id).then(rslt => {
        if (rslt == null) {
            return false;
        }
        else {
            rslt.password = "";
            return rslt;
        }
    });
};

module.exports.getAllUsers = () => {
    return User.find({}).then(rslt => {
        return rslt;
    });
};

module.exports.enroll = async (data) => {
    let isUserUpdated = await User.findById(data.userId).then(user => {
        user.enrollments.push({course_id: data.courseId});

        return user.save().then((usr, err) => {
            if (err) {
                return false;
            }
            else {
                return true;
            }
        });
    });

    let isCourseUpdated = await Course.findById(data.courseId).then(course => {
        course.enrollees.push({userID : data.userId});

        return course.save().then((course, err) => {
            if (err) {
                return false;
            }
            else {
                return true;
            }
        });
    });

    if (isUserUpdated && isCourseUpdated) {
        return true;
    }
    else {
        return false;
    }
};