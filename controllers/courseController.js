const Course = require("../models/Course");

module.exports.addCourse = (data) => {
    if (data.isAdmin) {
        let newCourse = new Course({
            name : data.course.name,
            description : data.course.description,
            price : data.course.price
        });
    
        return newCourse.save().then((course, err) => {
            if (err) {
                return `${err}`;
            }
            else {
                return {
                    message: "New course successfully created!"
                };
            }
        });
    }
    
    let message = Promise.resolve({
        message: "User must be an admin to access this!"
    });

    return message.then((value) => {
        return value;
    });
};

module.exports.getAllCourse = () => {
    return Course.find({}).then(rslt => {
        return rslt;
    });
};

module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(rslt => {
        return rslt;
    });
};

module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(rslt => {
        return rslt;
    });
};

module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    return Course.findByIdAndUpdate(reqParams.id, updatedCourse).then((course, err) => {
        if (err) {
            return err;
        }
        else {
            let message = `Successfully updated course ID - "${reqParams.id}"`;
            return message;
        }
    });
};

module.exports.archiveCourse = (reqParams) => {
    let archivedCourse = {
        isActive: false
    }
    return Course.findByIdAndUpdate(reqParams.id, archivedCourse).then((course, err) => {
        if (err) {
            return err;
        }
        else {
            return true
        }
    });
};