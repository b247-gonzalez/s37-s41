const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();
const port = 4004;

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://lng-athens:BVgvPPJIEu6qjm8V@zuitt-bootcamp.oquvder.mongodb.net/s37-s41?retryWrites=true&w=majority",
{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error"));
db.on("open", () => {
    console.log("Connected to MongoDB Atlas");
});

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(port, () => console.log(`Server is now running`));